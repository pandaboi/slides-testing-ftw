# Testing FTW (in Java)

## Was bedeutet testen in der Softwareentwicklung?
### Testpyramide (UI, Integrations-, Unittests)
* UI Tests (Selenium, Protractor, etc) simulieren User (kann super langsam sein)
* Integrationstests testen die einzelnen Module im Zusammenspiel (langsam)
* Unittests testen alle Module isoliert und unabhängig voneinander (easy, schnell)
    * Hilft uns die Logik einer Methode abzusichern
    * Dokumetation

## Wieso brauche ich es?
* Test beweist, dass die Anforderungen an das System erfüllt werden
* manuelles Testen vs. agiles Testen
* Sicherheit für stetiges Refactoring (saubere Architektur)
* Probleme frühzeitig erkennen
* Regression
* Code Dokumentation

## Tooling

### JUnit
* Testframework
* erleichtert automatisiertes Testen

#### Komponenten
* annotations
    * @Test vor testMethode() leitet einen Test ein
    * @Before, @After: setup()/tearDown() wird vor/nach jedem Test ausgeführt
    * @BeforeClass, @AfterClass: setup() wird einmal vor/nach Test ausgeführt (logging bevor ich DB tests machen will)
* mit asserts absichern
    * assertTrue(), assertFalse() 
    * assertEquals()
    * assertNull()
    * fail()
    * etc.
* @Test(expected = SomeException.class) erwartet, dass SomeException gefeuert wird (ganzem Test)
    * falls man genau nach einer Methodenaufruf eine Exception erwartet:
    ```
        try {
            mustThrowException();
            fail();
        } catch (Exception e) {
            // expected
            // check for exception properties
        }
    ```
* @Ignore (ignoriert Tests bzw. Klasse)
* @Parameter
    ```
        siehe public class SuperParameterizedTest Demo
    ```


### Mockito (Tasty mocking framework for unit tests in Java)
* Verhalten von Abhängigkeiten selbst bestimmen.
* Eine Klasse von Abhängigkeiten isolieren durch mocks
#### Komponenten
* Mocken mit: when()/given() (von Abhängigkeiten unabhängig machen)
* Behavior Testing: Aufrufe von Mocks überprüfen mit verify()  
* -> Behavior testing does not check the result of a method call, but it checks that a method is called with the right parameters
* annotations 
    * @Spy
    * @Mock
    * etc.
* fluent api
    * when(mock.call(anyObject())).thenReturn()
    * doNothing().when(mock).somMethod(anyObject()); (für void methoden) doThrow

### Konventionen
* ClassnameTest (-Test suffix)
* testMethod<ShouldDoSomething>() - sollte erklären was der test macht (siehe Code Doku)
* parallele package Struktur:
```
    |
    |--src
        |
        |- main
        |    |
        |    |- java  
        |       |
        |       |- package1
        |       |- package2
        |              
        |- test
             |
             |- java  
                |
                |- package1
                |- package2
```

### Teststrategien, -pattern
* Blackbox (Eingabe erstellen und Ausgabe mit definierter Resultat checken, interne Struktur ist egal)
    * add(1,2) == 3
* Whitebox (Aufrufe in einer Methode testen, addMethode())
    * udpate(FunnyThing ft){ ft.explode()} -> wurde FunnyThing#explode() genau einmal aufgerufen?
* Modularisiert/Isoliert - ein Test deckt nur eine Funktionaliät ab
* TDD ?
* Pair - Ping Pong ?

### Metriken
Ein Maßangabe zur Messung der Testfälle in Prozent

* Line Coverage (syntax - einzeiler)
* Path Coverage 
    * in jede Bedingung muss einmal gesprungen werden
* Condition Coverage (alle möglichen Bedingungswerte müssen abgecheckt werden)
    * if (a || b)
* Loop Coverage
    * jede Schleife wurde kein, einmal und mehrmals

(Tipp: Alle Fälle abdecken durch negative und positive Tests) siehe Beispiel isNeighbour()

### Zusammenfassung/Weiteres vorgehen
* Versionierung
* Tests sollen im besten Falle bei jedem Build laufen (lokal + server)
* Integration in Build-Tools Maven
* CI - Jenkins

## Fragen/Einführung in Tagesaufgabe

## Links etc.
* http://junit.org/junit4/
* http://mockito.org/
* Clean Code: A Handbook of Agile Software Craftsmanship
* http://www.vogella.com/
* http://junit.org/junit4/javadoc/latest/index.html
* http://site.mockito.org/mockito/docs/current/org/mockito/Mockito.html